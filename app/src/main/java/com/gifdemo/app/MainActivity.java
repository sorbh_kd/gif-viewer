package com.gifdemo.app;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.felipecsl.gifimageview.library.GifImageView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Saurabh on 12/9/2015.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Context context;

    private GifImageView gifImageView, gifImageView1;

    private boolean mFullScreenFlag = false;
    private File mFile;
    private Dialog dialog;
    private CountDownTimer mCount;

    //Notification Element
    NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        //Dailog to show GIF in full screen mode
        dialog = new Dialog(context,android.R.style.Theme_Black_NoTitleBar);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setFormat(PixelFormat.TRANSLUCENT);

        //Set Custom Title bar to alert dialog box
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//        dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);

        //SEt Custom View to Dilaog to show GIF in scroll
        View view = getLayoutInflater().inflate(R.layout.dialog_xml, null);
        gifImageView1 = (GifImageView) view.findViewById(R.id.gifImageView1);
        dialog.setContentView(view);

        gifImageView = (GifImageView) findViewById(R.id.gifImageView);
        gifImageView.setOnClickListener(this);
        findViewById(R.id.fileSelector).setOnClickListener(this);
    }


    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.fileSelector:
                new FileChooser(MainActivity.this).setFileListener(new FileChooser.FileSelectedListener() {
                    @Override
                    public void fileSelected(final File file) {
                        // do something with the file
                        String Fpath = file.getPath();
                        Log.d(TAG, "File Path : " + Fpath);

                        if (Fpath.endsWith("gif")) {
                            mFile = file;
                            byte[] bytes = getBytesFromFile(file);
                            gifImageView.setBytes(bytes);
                            gifImageView.startAnimation();
                        } else {
                            Toast.makeText(context, "Please select valid gif file", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).showDialog();

                break;

            case R.id.gifImageView:
                //Check if file is selected or not before go in full screen mode
                if (mFile != null) {

                    //setup the resources
                    byte[] bytes = getBytesFromFile(mFile);
                    gifImageView1.setBytes(bytes);
                    gifImageView1.startAnimation();

                    //Show Dialog
                    dialog.show();

                    //Show notification
                    showNotification();

                    mFullScreenFlag = true;

                    //Timer to shut down after 1 min if image is open in full mode
                    mCount = new CountDownTimer(60000, 1000) {

                        public void onTick(long millisUntilFinished) {

                        }

                        public void onFinish() {

                            if (mFullScreenFlag) {//Check if app is still in full screen mode

                                //Code to shut-down the phone
//                                Intent i = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
//                                i.putExtra("android.intent.extra.KEY_CONFIRM", true);
//                                startActivity(i);
                            }
                        }

                    };
                    mCount.start();
                }


                break;
        }
    }

    public byte[] getBytesFromFile(File file) {
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    @Override
    protected void onResume() {
        super.onResume();

        try{
            if(mFullScreenFlag && dialog != null)
                dialog.show();
        }catch (Exception e){

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try{
            if(mFullScreenFlag && dialog != null)
                dialog.dismiss();
        }catch (Exception e){

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            if (mFullScreenFlag) {//Check if app is in full screen mode or not
                mFullScreenFlag = false;

                //Remove the dialog
                dialog.dismiss();

                //Hide notification
                hideNotification();

                //reset the shutdown timer
                mCount.cancel();
                mCount = null;

                return true;
            }
            return super.onKeyDown(keyCode, event);
        }else{
            return super.onKeyDown(keyCode, event);
        }
    }

    public void showNotification(){
        NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.p2) // notification icon
                .setContentTitle("Title!") // title for notification
                .setContentText("Content Text") // message for notification
                .setAutoCancel(true);
        notificationManager.notify(0, mBuilder.build());
    }
    public void hideNotification(){
        notificationManager.cancelAll();
    }
}
